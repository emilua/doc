#!/bin/bash

branches=$(git ls-remote --heads https://gitlab.com/emilua/emilua.git | awk -F'/' '$NF ~ /^emilua-[0-9]+\.[0-9]+\.x$/ { sub(/\.x$/, "", $NF); print $NF }')

mkdir public

for branch in $branches; do
  git clone --depth=1 -b ${branch}.x https://gitlab.com/emilua/emilua.git
  
  bash emilua/doc/remove-xrefs-inplace.sh

  asciidoctor-epub3 -r asciidoctor-diagram -a ditaa-format=png -a plantuml-format=png -a source-highlighter=rouge -a icons=font -d book -o public/$branch.epub -a ebook-validate emilua/doc/all.adoc
  asciidoctor-pdf -r asciidoctor-diagram -a ditaa-format=svg -a plantuml-format=svg -a pdf-theme=theme/style.yml -d book -a source-highlighter=rouge -a icons=font -o public/$branch.pdf emilua/doc/all.adoc
  
  rm -rf emilua
done
